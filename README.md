This collection of code gatherd and manipulated the audio recordings used in the pipes sculpture.  
A simplified breakdown of process:  
- use Python and VimeoAPI to search for all zoom videos uploaded in a certain month using a special search query  
- gather and compile the unique URL ID (9 digits) of these videos  
- using a JS and PHP webapp, sift through these videos with a 'yes' and 'no' button  
- save 'yes' videos unique URL to a new file  
- Python script to download these videos and the scrape audio  
- compile the audio and pass it to SuperCollider  
- A SuperCollider granular synthesiser that cycles throguh clips 1 grain at time - I've never seen a granular synth engine like it before.  
- output to each speaker  
- Voila  

Find inside:  
- autostart.sh: Bash script for RaspberryPi running SuperCollider and setting up Jack Audio to create audio interface from 2 seperate interfaces.  
- PIAS.scd: SuperCollider code for the pipes. Readability and ease of on the fly bug fixing prioritised.  
- JupyterPython Folder:   
    vimeoLinkCollector.ipynb:  Python script to gather unique URL ID codes for Zoom videos from within the correct month, using
    a specific search query  
    downloadAudioStrip.ipynb: Python script for downloading and scraping audio from selected vimeo videos  
- CheckerWebApp:  
        A 2 page PHP & JS website that I used to sift through all the vimeo video I gathered and select the good/relevant ones.  
  
Please remember:  
This code is a practical cog to create a piece of art. It is meaningless in itself.   
