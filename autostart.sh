#!/bin/bash

jackd -d alsa -d hw:2 -m -r -o 8 -r 48000 &

sleep 1 &

alsa_out -d hw:3 -c 8 -r 48000 &

sleep 1 &

sclang /////location//////

jack_connect SuperCollider:out_9 alsa_out:playback_1 &
jack_connect SuperCollider:out_10 alsa_out:playback_2 &
jack_connect SuperCollider:out_11 alsa_out:playback_3 &
jack_connect SuperCollider:out_12 alsa_out:playback_4 &
jack_connect SuperCollider:out_13 alsa_out:playback_5 &
jack_connect SuperCollider:out_14 alsa_out:playback_6 &
jack_connect SuperCollider:out_15 alsa_out:playback_7 &
jack_connect SuperCollider:out_16 alsa_out:playback_8 &
